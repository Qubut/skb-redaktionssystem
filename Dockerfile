FROM node:16.14
ARG WORK_DIR=/backend
ENV NODE_ENV=production
WORKDIR ${WORK_DIR}
COPY ./package.json ./
ENV PATH ${WORK_DIR}/node_modules/.bin:$PATH
RUN npm config set network-timeout 600000 -g
RUN npm i -f
COPY ./ .
RUN npm run build
EXPOSE 1337
CMD ["npm", "start"]

