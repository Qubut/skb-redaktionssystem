export default ({ env }) => ({
  slugify: {
    enabled: true,
    config: {
      contentTypes: {
        menu: {
          field: "slug",
          references: "slug",
        },
        aussteller: {
          field: "slug",
          references: "firma",
        },
      },
    },
  },
  transformer: {
    enabled: true,
    config: {
      prefix: "/api/",
      responseTransforms: {
        removeAttributesKey: true,
        removeDataKey: true,
      },
    },
  },

  menus: {
    config: {
      maxDepth: 3,
    },
  },

});
