export {};
declare global {
  type Aussteller = {
    id: number;
    firma: string;
    telefon: string;
    homepage: string;
    strasse: string;
    adresszusatz: string;
    plz: string;
    land: string;
    aktualisieren: boolean;
    ort: string;
    stellenausschreibungs: any[];
    printmedia: any;
    onlinemedien: any;
    messestand: Messestand;
    fachvortrag: any;
    ansprechpartner: Ansprechpartner;
    ausstellerid: number;
  };
  
  type Ansprechpartner = {
    id: number;
    vorname: string;
    name: string;
    titel: string;
    anrede: string;
    telefon: string;
    email: string;
  };
  type Messestand = {
    id: number;
    messestand: "Messestand B" | "Messestand A";
  };
  type Printmedia = {};
}
