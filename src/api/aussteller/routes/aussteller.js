'use strict';

/**
 * aussteller router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::aussteller.aussteller');
