'use strict';

/**
 * aussteller service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::aussteller.aussteller');
