/**
 * aussteller controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::aussteller.aussteller');
