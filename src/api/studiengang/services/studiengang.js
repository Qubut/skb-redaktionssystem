'use strict';

/**
 * studiengang service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::studiengang.studiengang');
