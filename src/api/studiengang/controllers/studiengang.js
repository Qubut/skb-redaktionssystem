'use strict';

/**
 *  studiengang controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::studiengang.studiengang');
