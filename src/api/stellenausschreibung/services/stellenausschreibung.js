'use strict';

/**
 * stellenausschreibung service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::stellenausschreibung.stellenausschreibung');
