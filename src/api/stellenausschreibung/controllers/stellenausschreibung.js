'use strict';

/**
 *  stellenausschreibung controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::stellenausschreibung.stellenausschreibung');
