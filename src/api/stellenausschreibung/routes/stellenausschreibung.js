'use strict';

/**
 * stellenausschreibung router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::stellenausschreibung.stellenausschreibung');
