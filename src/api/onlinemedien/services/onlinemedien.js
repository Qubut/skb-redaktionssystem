'use strict';

/**
 * onlinemedien service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::onlinemedien.onlinemedien');
