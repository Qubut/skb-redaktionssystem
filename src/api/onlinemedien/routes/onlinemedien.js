'use strict';

/**
 * onlinemedien router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::onlinemedien.onlinemedien');
