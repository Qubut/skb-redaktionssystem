/**
 * onlinemedien controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::onlinemedien.onlinemedien');
