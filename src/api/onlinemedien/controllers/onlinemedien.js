'use strict';

/**
 *  onlinemedien controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::onlinemedien.onlinemedien');
