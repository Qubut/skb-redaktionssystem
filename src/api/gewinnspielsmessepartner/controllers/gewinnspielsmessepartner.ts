'use strict';

/**
 *  gewinnspielsmessepartner controller
 */

import { factories } from '@strapi/strapi';

 export default factories.createCoreController('api::gewinnspielsmessepartner.gewinnspielsmessepartner');
