'use strict';

/**
 * gewinnspielsmessepartner router.
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::gewinnspielsmessepartner.gewinnspielsmessepartner');
