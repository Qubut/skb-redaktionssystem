'use strict';

/**
 * gewinnspielsmessepartner service.
 */

import { factories } from '@strapi/strapi';

export default  factories.createCoreService('api::gewinnspielsmessepartner.gewinnspielsmessepartner');
