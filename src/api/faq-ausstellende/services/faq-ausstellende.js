'use strict';

/**
 * faq-ausstellende service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::faq-ausstellende.faq-ausstellende');
