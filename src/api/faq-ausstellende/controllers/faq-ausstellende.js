'use strict';

/**
 *  faq-ausstellende controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::faq-ausstellende.faq-ausstellende');
