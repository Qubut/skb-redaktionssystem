'use strict';

/**
 * faq-ausstellende router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::faq-ausstellende.faq-ausstellende');
