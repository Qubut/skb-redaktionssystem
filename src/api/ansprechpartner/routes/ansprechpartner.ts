/**
 * ansprechpartner router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::ansprechpartner.ansprechpartner');
