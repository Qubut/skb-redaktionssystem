'use strict';

/**
 * faq-besuchende service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::faq-besuchende.faq-besuchende');
