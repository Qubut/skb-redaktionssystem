'use strict';

/**
 * faq-besuchende router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::faq-besuchende.faq-besuchende');
