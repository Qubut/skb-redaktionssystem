'use strict';

/**
 *  faq-besuchende controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::faq-besuchende.faq-besuchende');
