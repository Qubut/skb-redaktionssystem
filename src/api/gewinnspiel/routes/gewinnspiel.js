'use strict';

/**
 * gewinnspiel router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::gewinnspiel.gewinnspiel');
