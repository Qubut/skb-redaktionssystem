'use strict';

/**
 * gewinnspiel service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::gewinnspiel.gewinnspiel');
