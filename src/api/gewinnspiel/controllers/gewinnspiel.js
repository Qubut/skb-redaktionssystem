'use strict';

/**
 *  gewinnspiel controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::gewinnspiel.gewinnspiel');
