'use strict';

/**
 *  premium-aussteller controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::premium-aussteller.premium-aussteller');
