'use strict';

/**
 * premium-aussteller service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::premium-aussteller.premium-aussteller');
