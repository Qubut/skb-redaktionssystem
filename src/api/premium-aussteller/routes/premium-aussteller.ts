/**
 * premium-aussteller router
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::premium-aussteller.premium-aussteller');
