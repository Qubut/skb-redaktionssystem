'use strict';

/**
 * premium-aussteller router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::premium-aussteller.premium-aussteller');
