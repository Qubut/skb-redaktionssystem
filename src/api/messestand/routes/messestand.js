'use strict';

/**
 * messestand router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::messestand.messestand');
