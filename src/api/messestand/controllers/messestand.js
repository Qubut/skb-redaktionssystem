'use strict';

/**
 *  messestand controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::messestand.messestand');
