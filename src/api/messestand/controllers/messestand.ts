/**
 * messestand controller
 */

import { factories } from '@strapi/strapi'

export default factories.createCoreController('api::messestand.messestand');
