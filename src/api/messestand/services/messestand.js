'use strict';

/**
 * messestand service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::messestand.messestand');
