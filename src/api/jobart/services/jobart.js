'use strict';

/**
 * jobart service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::jobart.jobart');
