/**
 * jobart service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::jobart.jobart');
