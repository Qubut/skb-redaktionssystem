'use strict';

/**
 *  jobart controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::jobart.jobart');
