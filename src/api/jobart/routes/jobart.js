'use strict';

/**
 * jobart router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::jobart.jobart');
