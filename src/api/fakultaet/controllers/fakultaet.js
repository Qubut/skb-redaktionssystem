'use strict';

/**
 *  fakultaet controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::fakultaet.fakultaet');
