'use strict';

/**
 * fakultaet service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::fakultaet.fakultaet');
