'use strict';

/**
 * fakultaet router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::fakultaet.fakultaet');
