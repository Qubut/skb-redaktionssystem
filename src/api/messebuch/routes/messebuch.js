'use strict';

/**
 * messebuch router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::messebuch.messebuch');
