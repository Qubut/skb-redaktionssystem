'use strict';

/**
 * messebuch service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::messebuch.messebuch');
