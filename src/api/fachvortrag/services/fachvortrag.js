'use strict';

/**
 * fachvortrag service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::fachvortrag.fachvortrag');
