/**
 * fachvortrag service
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::fachvortrag.fachvortrag');
