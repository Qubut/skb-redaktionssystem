'use strict';

/**
 * fachvortrag router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::fachvortrag.fachvortrag');
