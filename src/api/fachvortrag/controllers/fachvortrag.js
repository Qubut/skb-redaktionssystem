'use strict';

/**
 *  fachvortrag controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::fachvortrag.fachvortrag');
