'use strict';

/**
 *  messepartner controller
 */

import { factories } from '@strapi/strapi';

 export default factories.createCoreController('api::messepartner.messepartner');
