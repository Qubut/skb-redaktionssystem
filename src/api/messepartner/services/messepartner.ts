'use strict';

/**
 * messepartner service.
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreService('api::messepartner.messepartner');
