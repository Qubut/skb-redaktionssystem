'use strict';

/**
 * messepartner router.
 */

import { factories } from '@strapi/strapi';

export default factories.createCoreRouter('api::messepartner.messepartner');
