'use strict';

/**
 *  befragung-fuer-besuchende controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::befragung-fuer-besuchende.befragung-fuer-besuchende');
