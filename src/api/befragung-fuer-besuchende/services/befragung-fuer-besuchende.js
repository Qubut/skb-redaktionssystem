'use strict';

/**
 * befragung-fuer-besuchende service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::befragung-fuer-besuchende.befragung-fuer-besuchende');
