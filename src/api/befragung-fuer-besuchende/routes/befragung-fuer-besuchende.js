'use strict';

/**
 * befragung-fuer-besuchende router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::befragung-fuer-besuchende.befragung-fuer-besuchende');
