'use strict';

/**
 *  karriereservice controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::karriereservice.karriereservice');
