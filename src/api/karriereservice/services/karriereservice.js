'use strict';

/**
 * karriereservice service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::karriereservice.karriereservice');
