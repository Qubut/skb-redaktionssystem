'use strict';

/**
 * printmedia service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::printmedia.printmedia');
