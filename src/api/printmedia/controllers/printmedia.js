'use strict';

/**
 *  printmedia controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::printmedia.printmedia');
