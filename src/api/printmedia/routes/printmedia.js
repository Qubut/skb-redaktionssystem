'use strict';

/**
 * printmedia router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::printmedia.printmedia');
